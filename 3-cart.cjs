const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        }, {
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]


/*

Q1. Find all the items with price more than $65.
Q2. Find all the items where quantity ordered is more than 1.
Q.3 Get all items which are mentioned as fragile.
Q.4 Find the least and the most expensive item for a single quantity.
Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file

*/


//=================================================================================



function gettingNestedData(products) {

    const keys = Object.keys(products);

    const result = keys.reduce((obj, key) => {
        const product = products[key];

        if (Array.isArray(product) === false) {
            obj[key] = product;
        } else {
            const returnObject = product.reduce((newObj, curr) => {
                return {
                    ...newObj, ...curr
                };
            }, {});
            obj = {
                ...obj, ...returnObject
            }
        };

        return obj;
    }, {});
    return result;
}

// console.log(gettingNestedData(products[0]));




//===================================================================================

// Q1. Find all the items with price more than $65.

function itemsMoreThan65Doller(products) {
    const allProducts = gettingNestedData(products);

    const keys = Object.keys(allProducts);

    const result = keys.reduce((obj, key) => {
        const product = allProducts[key];
        const price = +product.price.replace("$", "");
        if (price > 65) {
            obj[key] = product;
        }
        return obj;
    }, {});
    return result;
}


console.log(itemsMoreThan65Doller(products[0]));


//=================================================================================

// Q2. Find all the items where quantity ordered is more than 1.



function quantityItemsMoreThanOne(products) {

    const allProducts = gettingNestedData(products);
    const keys = Object.keys(allProducts);

    const result = keys.reduce((obj, key) => {
        const product = allProducts[key];
        let { quantity } = product;
        if (quantity > 1) {
            obj[key] = product;
        }
        return obj;
    }, {});
    return result;
}


console.log(quantityItemsMoreThanOne(products[0]));





//=================================================================================


// Q.3 Get all items which are mentioned as fragile.


function allItemsAsFragile(products) {
    const allProducts = gettingNestedData(products);
    const keys = Object.keys(allProducts);

    const result = keys.reduce((obj, key) => {
        const product = allProducts[key];
        const { type } = product;
        if (type === "fragile") {
            obj[key] = product;
        }
        return obj;
    }, {});
    return result;
}

console.log(allItemsAsFragile(products[0]));



//=================================================================================

// Q.4 Find the least and the most expensive item for a single quantity.


function findLeastAndMostExpensiveItem(products) {
    const allProducts = gettingNestedData(products);
    const keys = Object.keys(allProducts);

    const result = keys.filter(key => {
        const product = allProducts[key];
        const { quantity } = product;
        return quantity === 1;
    })
        .reduce((arr, curr) => {
            const product = allProducts[curr];
            const price = +product.price.replace("$", "");
            if (arr[0] < price) {
                arr[0] = price;
                arr[1] = {};
                arr[1][curr] = product;
            }
            return arr
        }, [0, {}]);
    return result[1];
}


console.log(findLeastAndMostExpensiveItem(products[0]));


//==============================================================================

// Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)


function groupItemsBasedOnState(product) {
    const allProducts = gettingNestedData(product);
    const keys = Object.keys(allProducts);

    const result = keys.reduce((arr, key) => {
        const product = allProducts[key];
        if (key === "shampoo" || key === "Hair-oil") {
            arr[1].liquid[key] = product;
        }else if (key === "watch" || key === "comb" || key === "spoons" || key === "glasses" || key === "cooker") {
            arr[0].solid[key] = product;
        }else {
            arr[2].gas[key] = product;
        };
        return arr;

    }, [ {solid : {}}, {liquid: {}}, {gas: {}} ]);

    const ans = result.map(data => {
        const solid = data.solid;
        const liquid = data.liquid;
        const gas = data.gas;

        if(solid){
            return { solid : data["solid"]};
        }else if(liquid){
            return { liquid : data["liquid"]};
        }else if(gas){
            return { gas : data["gas"]};
        };
    });
    return ans;
}

console.log(groupItemsBasedOnState(products[0]));



//==============================================================================





// ======= Below code is just for practicing in different way.========

//========================================================================


// Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)




// function mainFunction(products) {

//     function groupItemsBasedOnState(products) {

//         const keys = Object.keys(products);

//         const result = keys.reduce((obj, key) => {
//             const product = products[key];

//             if (Array.isArray(product) === false) {
//                 if (key === "shampoo" || key === "Hair-oil") {
//                     obj.liquid.push([key, product]);
//                 } else {
//                     obj.solid.push([key, product]);
//                 }
//             } else {
//                 const returnObject = product.reduce((obj, curr) => {
//                     return {
//                         ...obj, ...curr
//                     };
//                 }, {});
//                 const newObj = groupItemsBasedOnState(returnObject);
//                 obj.solid.push(...newObj.solid);
//                 obj.liquid.push(...newObj.liquid);
//                 obj.gas.push(...newObj.gas);
//             };
//             return obj;
//         }, { solid: [], liquid: [], gas: [] });
//         return result;
//     };

//     const result = groupItemsBasedOnState(products[0]);
//     result.solid = Object.fromEntries(result.solid);
//     result.liquid = Object.fromEntries(result.liquid);
//     result.gas = Object.fromEntries(result.gas);

//     return result;
// }



// console.log(mainFunction(products));


//====================================================================